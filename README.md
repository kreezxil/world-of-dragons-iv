# World of Dragons IV
## What is this?
Simply put it's the issue tracker for the pack.

## I want to report a bug or request
Excellent, that's what I created this for, use the issue tracker above and to the left by clicking on [issues](https://gitlab.com/kreezxil/world-of-dragons-iv/-/issues).
## Will this have a wiki?
Maybe, maybe not. My plans are to have everything about the pack inside of the FTB QUESTS pages. 
## Can we talk to you or find people to play with somewhere?
Yes, you can join me on my Discord at https://discord.gg/ngNQjT5

[![World of Dragons IV Promo](https://media.forgecdn.net/attachments/794/727/bh_wod4_promo.png)](https://kreezcraft.com/wodiv)