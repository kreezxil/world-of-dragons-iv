# Crash Report
Congratulations you've found an unintended feature. I will need a few things from you in the following order. The first is the most important and the last is least important.

# The Crash Report File
- Typically you can get it by right clicking your copy of my pack, choosing open folder, and then entering the craash-reports folder. The last file created is the one I need. Simply drop it on to this form to upload it.
Like right here: ->

# The Log File
- You will find it in the logs folder. It will say "latest" on windows and everywhere else (mac, linux) it will say "latest.log"
Same as above drop it here: ->

# What you were doing when the crash happened
- I know, i feel you. But if I know exactly what you were clicking on, looking at, etc I might be able to duplicate it on my test copy and get this fixed sooner rather than later.

# And finally a screenshot or video.
- You can drop the screen shot here and if it's a video share the link.
Droppy drop time: ->